TEX     := xelatex
HTMLC   := pandoc
TARGETS := example.pdf

.PHONY: all clean reset
all: $(TARGETS)

%.pdf: %.tex
	$(TEX) $< -jobname=$@

%.html: %.tex
	$(HTMLC) -t html $< -o $@

clean:; rm -f $(shell cat .gitignore | sed 's/\*\*\//.\//g')
reset: clean; rm -f $(TARGETS)
